import React from 'react';
import NoSSR from 'react-no-ssr';
import styled from 'styled-components';

const Container = styled.span`
    top: 12px;
    position: relative;
    margin-left: 1rem;
`;

const Spinner = () => (
    <NoSSR>
        <Container />
    </NoSSR>
);

// Spinner.propTypes = MaterialSpinner.propTypes;

export default Spinner;
