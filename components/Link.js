import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

const StyledLink = ({ active, className, children, href, prefetch, ...props }) => (
    <Link href={href} prefetch={prefetch}>
        <a className={className} {...props}>
            {children}
        </a>
    </Link>
);

StyledLink.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    prefetch: PropTypes.bool,
};

export default StyledLink;
