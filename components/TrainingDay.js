import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { translate } from 'react-polyglot';

import Training from './Training';

const Container = styled.div`
    flex-grow: 1;
    flex-basis: 0;
    padding: 1rem 1rem;
`;

const TrainingContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

const TrainingTitle = styled.h2`
    font-weight: bold;
    border-bottom: 1px solid black;
`;

const TrainingDay = ({ day, first, t }) => (
    <Container key={day.day} first={first}>
        <TrainingTitle>{t(day.day.toLowerCase())}</TrainingTitle>
        <TrainingContainer>
            {day.trainings.map((training, index) => (
                <Training key={training.id} last={index === day.trainings.length - 1} training={training} />
            ))}
        </TrainingContainer>
    </Container>
);

TrainingDay.propTypes = {
    day: PropTypes.object.isRequired,
    first: PropTypes.bool,
    t: PropTypes.func.isRequired,
};

export default translate()(TrainingDay);
