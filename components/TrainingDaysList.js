import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import compose from 'recompose/compose';
import withProps from 'recompose/withProps';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import getContext from 'recompose/getContext';

import withTrainings from '../data/withTrainings';
import TrainingDay from './TrainingDay';
import TrainingEdition from './TrainingEdition';
import dpcPropTypes from '../lib/propTypes';

import { Button } from '../components/forms';

const EditionContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    margin-top: 0.5rem;
    flex-grow: 0;
    flex-shrink: 0;
`;

const TrainingDaysList = ({ add, canAdd, toggleAdd, trainingDays = [] }) => (
    <div>
        <EditionContainer>
            {canAdd && (
                <Button primary onClick={toggleAdd}>
                    Ajouter
                </Button>
            )}
            <TrainingEdition toggleEdition={toggleAdd} show={add} />
        </EditionContainer>

        {trainingDays.map((trainingDay, index) => (
            <TrainingDay key={trainingDay.day} day={trainingDay} first={index === 0} />
        ))}
    </div>
);

TrainingDaysList.propTypes = {
    add: PropTypes.bool.isRequired,
    canAdd: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    toggleAdd: PropTypes.func.isRequired,
    trainingDays: PropTypes.arrayOf(PropTypes.object),
};

export default compose(
    getContext({ user: dpcPropTypes.user }),
    withProps(({ user }) => ({ canAdd: user ? !!user.permissions.find(p => p.type === 'EDIT_TRAININGS') : false })),
    withState('add', 'setAdd', false),
    withHandlers({
        toggleAdd: ({ add, setAdd, canAdd }) => () => {
            if (!canAdd) return;
            setAdd(!add);
        },
    }),
    withTrainings,
)(TrainingDaysList);
