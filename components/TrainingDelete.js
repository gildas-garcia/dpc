import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import styledProperty from 'styled-property';
import ReactModal from 'react-modal';

import { Button } from './forms';

const Modal = styled(ReactModal)`
    position: absolute;
    top: 40px;
    left: 40px;
    right: 40px;
    border: 1px solid rgb(204, 204, 204);
    background: rgb(255, 255, 255);
    overflow: auto;
    border-radius: 4px;
    outline: none;
    padding: 20px;
`;

const ListItem = styled.li`margin: 0.5rem 0 0 0;`;

const ModalWithOverlay = styledProperty(Modal, 'overlayClassName')`
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(255, 255, 255, 0.75);
    z-index: 10000;
`;

const TrainingDelete = ({ onConfirm, onCancel, show, training }) => (
    <ModalWithOverlay contentLabel="Suppression d'un entrainement" isOpen={show}>
        <p>Voulez-vous vraiment supprimer l{"'"}entraînement suivant ?</p>
        <ul>
            <ListItem>Jour: {training.day}</ListItem>
            <ListItem>Public: {training.public}</ListItem>
            <ListItem>Horaires: {training.time}</ListItem>
            <ListItem>Lieu: {training.location}</ListItem>
            <ListItem>Prix: {training.price}</ListItem>
        </ul>
        <Button primary onClick={onConfirm}>
            Confirmer
        </Button>
        <Button onClick={onCancel}>Annuler</Button>
    </ModalWithOverlay>
);

TrainingDelete.propTypes = {
    onConfirm: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired,
    training: PropTypes.object.isRequired,
};

export default TrainingDelete;
