import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import styled from 'styled-components';
import { gql, graphql } from 'react-apollo';
import { Checkbox, CheckboxGroup } from 'react-checkbox-group';
import { Days } from '../data/withTrainings';
import { translate } from 'react-polyglot';

import withValidation from '../lib/withValidation';
import { Button, Error, Form, FormGroup, TextInput, Submit, Success } from './forms';
import { ModalWithOverlay } from './Modals';
import Spinner from './Spinner';

const CheckBoxLabel = styled.label`margin-right: 1rem;`;

const TrainingEdition = ({
    isValid,
    isSubmitting,
    onFieldChange,
    onSubmit,
    show,
    submitFailed,
    submitSucceeded,
    t,
    toggleEdition,
    values: { days, public: audience, time, location, yearPrice },
    validation,
}) => (
    <ModalWithOverlay contentLabel="Modification d'un entrainement" isOpen={show}>
        <Form onSubmit={onSubmit}>
            <FormGroup>
                <CheckboxGroup
                    name="days"
                    onChange={values => onFieldChange({ target: { name: 'days', value: values } })}
                    value={days}
                >
                    {Object.keys(Days).map(day => (
                        <CheckBoxLabel key={day}>
                            <Checkbox value={day} /> {t(day.toLowerCase())}
                        </CheckBoxLabel>
                    ))}
                </CheckboxGroup>
                {validation.days.touched && validation.days.errors && <Error>{validation.days.errors[0]}</Error>}
            </FormGroup>
            <FormGroup>
                <label htmlFor="public">Public</label>
                <TextInput id="public" name="public" type="text" required value={audience} onChange={onFieldChange} />
                {validation.public.touched && validation.public.errors && <Error>{validation.public.errors[0]}</Error>}
            </FormGroup>
            <FormGroup>
                <label htmlFor="time">Horaires</label>
                <TextInput id="time" name="time" type="text" required value={time} onChange={onFieldChange} />
                {validation.time.touched && validation.time.errors && <Error>{validation.time.errors[0]}</Error>}
            </FormGroup>
            <FormGroup>
                <label htmlFor="location">Lieu</label>
                <TextInput
                    id="location"
                    name="location"
                    type="text"
                    required
                    value={location}
                    onChange={onFieldChange}
                />
                {validation.location.touched &&
                validation.location.errors && <Error>{validation.location.errors[0]}</Error>}
            </FormGroup>
            <FormGroup>
                <label htmlFor="yearPrice">Tarif à l{"'"}année</label>
                <TextInput
                    id="yearPrice"
                    name="yearPrice"
                    type="number"
                    required
                    value={yearPrice}
                    onChange={onFieldChange}
                />
                {validation.yearPrice.touched &&
                validation.yearPrice.errors && <Error>{validation.yearPrice.errors[0]}</Error>}
            </FormGroup>
            <FormGroup>
                <Submit primary disabled={!isValid || isSubmitting}>
                    Enregistrer
                </Submit>
                <Spinner show={isSubmitting} />
                {submitSucceeded && <Success inline>Vous êtes connecté !</Success>}
                {submitFailed && <Error inline>Une erreur est survenue...</Error>}
                <Button onClick={toggleEdition}>Annuler</Button>
            </FormGroup>
        </Form>
    </ModalWithOverlay>
);

TrainingEdition.propTypes = {
    values: PropTypes.shape({
        days: PropTypes.arrayOf(PropTypes.string),
        public: PropTypes.string,
        time: PropTypes.string,
        location: PropTypes.string,
        price: PropTypes.number,
    }),
    isSubmitting: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
    onFieldChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    submitFailed: PropTypes.bool.isRequired,
    submitSucceeded: PropTypes.bool.isRequired,
    validation: PropTypes.shape({
        days: PropTypes.shape({
            touched: PropTypes.bool.isRequired,
            errors: PropTypes.arrayOf(PropTypes.string),
        }),
        public: PropTypes.shape({
            touched: PropTypes.bool.isRequired,
            errors: PropTypes.arrayOf(PropTypes.string),
        }),
        time: PropTypes.shape({
            touched: PropTypes.bool.isRequired,
            errors: PropTypes.arrayOf(PropTypes.string),
        }),
        location: PropTypes.shape({
            touched: PropTypes.bool.isRequired,
            errors: PropTypes.arrayOf(PropTypes.string),
        }),
        yearPrice: PropTypes.shape({
            touched: PropTypes.bool.isRequired,
            errors: PropTypes.arrayOf(PropTypes.string),
        }),
    }),
    show: PropTypes.bool.isRequired,
    t: PropTypes.func.isRequired,
    training: PropTypes.object,
    toggleEdition: PropTypes.func.isRequired,
};

const updateTrainingQuery = gql`
    mutation updateTraining(
        $id: ID!
        $days: [Days!]
        $public: String!
        $location: String!
        $time: String!
        $yearPrice: Int!
    ) {
        updateTraining(id: $id, days: $days, public: $public, location: $location, time: $time, yearPrice: $yearPrice) {
            id
            days
            public
            location
            time
            yearPrice
        }
    }
`;

const createTrainingQuery = gql`
    mutation createTraining($days: [Days!], $public: String!, $location: String!, $time: String!, $yearPrice: Int!) {
        createTraining(days: $days, public: $public, location: $location, time: $time, yearPrice: $yearPrice) {
            id
            days
            public
            location
            time
            yearPrice
        }
    }
`;

export default compose(
    graphql(updateTrainingQuery, { name: 'updateTraining' }),
    graphql(createTrainingQuery, { name: 'createTraining', options: { refetchQueries: ['allTrainings'] } }),
    withValidation(
        {
            days: {
                presence: { message: 'Requis' },
            },
            public: {
                presence: { message: 'Requis' },
            },
            location: {
                presence: { message: 'Requis' },
            },
            time: {
                presence: { message: 'Requis' },
            },
            yearPrice: {
                presence: { message: 'Requis' },
            },
        },
        ({ yearPrice, ...variables }, { training, toggleEdition, createTraining, updateTraining }) =>
            training
                ? updateTraining({
                      variables: { ...variables, id: training.id, yearPrice: parseInt(yearPrice) },
                  }).then(({ errors }) => {
                      if (errors && errors.length > 0) {
                          throw new Error(errors[0].message);
                      }

                      toggleEdition();
                      return true;
                  })
                : createTraining({ variables: { ...variables, yearPrice: parseInt(yearPrice) } }).then(({ errors }) => {
                      if (errors && errors.length > 0) {
                          throw new Error(errors[0].message);
                      }

                      toggleEdition();
                      return true;
                  }),
        ({ training }) => training || {},
    ),
    translate(),
)(TrainingEdition);
