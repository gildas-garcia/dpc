import styled from 'styled-components';
import { lighten } from 'polished';
import Textarea from 'react-textarea-autosize';
import Link from './Link';

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    width: 100%;
    box-sizing: border-box;
`;

export const FormGroup = styled.div`
    box-sizing: border-box;
    margin-top: 1rem;
`;

export const TextInput = styled.input`
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-sizing: border-box;
    width: 100%;
    padding: 0.5rem 1rem;
    margin-top: 0.5rem;
    outline: none;

    &:focus {
        border-color: ${lighten(0.2, 'rgb(255, 165, 0)')};
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px ${lighten(0.2, 'rgb(255, 165, 0)')};
    }
`;

export const TextArea = TextInput.withComponent(Textarea);

export const Button = styled.button`
    color: black;
    background-color: ${({ disabled, primary, secondary }) => {
        if (disabled) {
            if (primary) {
                return lighten(0.2, 'rgb(255, 165, 0)');
            }
            return 'inherit';
        }

        if (primary) {
            return 'rgb(255, 165, 0)';
        }
        if (secondary) {
            return 'rgb(255, 255, 255)';
        }

        return 'inherit';
    }};
    border: 10px;
    border-radius: 2px;
    box-sizing: border-box;
    cursor: pointer;
    display: inline-block;
    outline: none;
    transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
    padding: 0.5rem 1rem;
    text-transform: uppercase;
    font-size: 1.1rem;
    text-decoration: none;
    height: 38px;

    &:hover {
        background: ${({ disabled }) => !disabled && lighten(0.2, 'rgb(255, 165, 0)')};
    }
`;

export const LinkButton = Button.withComponent(Link);
export const AnchorButton = Button.withComponent('a');

export const Submit = Button;

export const Message = styled.p`
    display: ${({ inline }) => (inline ? 'inline-block' : 'block')};
    margin-left: ${({ inline }) => (inline ? '1rem' : '0')};
`;

export const Error = Message.extend`color: red;`;

export const Success = Message.extend`color: green;`;
