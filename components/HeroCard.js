import styled from 'styled-components';

export default styled.div`
    background-color: rgba(255, 255, 255, 0.7);
    display: flex;
    flex-direction: column;
    box-shadow: 0 0 1rem black;
    margin: 2rem 0;
    padding: 2rem 1rem;
`;
