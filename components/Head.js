import React from 'react';
import NextHead from 'next/head';
import PropTypes from 'prop-types';
import withIntl from '../lib/withIntl';

const defaultDescription = 'seo.description';
const defaultOGURL = 'https://parkour-dijon.com';
const defaultOGImage = '';

const Head = ({ title, description, url, ogImage, t }) => (
    <NextHead>
        <meta charSet="UTF-8" />
        <title>DPC - {title || ''}</title>
        <meta name="description" content={description || t(defaultDescription)} />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width" />
        <link rel="icon" sizes="192x192" href="/static/touch-icon.png" />
        <link rel="apple-touch-icon" href="/static/touch-icon.png" />
        <link rel="mask-icon" href="/static/favicon-mask.svg" color="#49B882" />
        <link rel="icon" href="/static/favicon.ico" />
        <meta property="og:url" content={url || defaultOGURL} />
        <meta property="og:title" content={`DPC - ${title}` || ''} />
        <meta property="og:description" content={description || t(defaultDescription)} />
        <meta name="twitter:site" content={url || defaultOGURL} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:image" content={ogImage || defaultOGImage} />
        <meta property="og:image" content={ogImage || defaultOGImage} />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
    </NextHead>
);

Head.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    url: PropTypes.string,
    ogImage: PropTypes.string,
    t: PropTypes.func.isRequired,
};

export default withIntl(Head);
