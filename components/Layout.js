import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Head from './Head';
import Nav from './Nav';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 992px;
    margin: auto;
`;

const Content = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 6rem;
`;

const Layout = ({ children, title }) => (
    <Container>
        <Head title={title} />
        <Nav />
        <Content>{children}</Content>
    </Container>
);

Layout.propTypes = {
    children: PropTypes.node.isRequired,
    title: PropTypes.string.isRequired,
};

export default Layout;
