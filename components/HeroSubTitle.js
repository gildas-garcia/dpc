import styled from 'styled-components';

export default styled.p`
    color: orange;
    font-size: 2rem;
    text-align: center;
    text-shadow: 0 0 1rem black;
`;
