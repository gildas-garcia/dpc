import styled from 'styled-components';

export default styled.hr`
    margin: 1rem 0;
`;
