import styled from 'styled-components';

export default styled.div`
    background-color: rgba(255, 255, 255, 0.7);
    padding: 0.1rem 0.5rem;
    margin-top: 0.5rem;
    box-sizing: border-box;
    box-shadow: 0px 0px 2px 2px rgba(0, 0, 0, 0.2);
    position: relative;
`;
