import React from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import { gql, graphql } from 'react-apollo';
import compose from 'recompose/compose';
import withHandlers from 'recompose/withHandlers';
import withState from 'recompose/withState';
import styled from 'styled-components';
import PulseLoader from 'react-spinners/dist/spinners/PulseLoader';

import { ModalWithOverlay } from './Modals';
import { Button } from './forms';
import { loadStaticQuery } from './Static';

const Container = styled.div`
    display: flex;
    position: relative;
`;
const Section = styled.div`
    width: 50%;
    margin: 0 1rem 1rem;
`;
const TextArea = styled.textarea`
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    resize: none;
`;

const StaticEdition = ({ currentContent, handleChange, onClose, requestUpdate, saving }) => (
    <ModalWithOverlay contentLabel="" isOpen>
        <Container>
            <Section>
                <TextArea defaultValue={currentContent} onChange={handleChange} />
            </Section>
            <Section>
                <ReactMarkdown source={currentContent} />
            </Section>
        </Container>
        <Button primary disabled={saving} onClick={requestUpdate}>
            {saving ? <PulseLoader size={5} /> : <span>Enregistrer</span>}
        </Button>
        <Button onClick={onClose} disabled={saving}>
            Annuler
        </Button>
    </ModalWithOverlay>
);

StaticEdition.propTypes = {
    currentContent: PropTypes.string,
    handleChange: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    requestUpdate: PropTypes.func.isRequired,
    saving: PropTypes.bool.isRequired,
};

const mutation = gql`
    mutation updateStatic($id: ID!, $content: String!) {
        updateStatic(id: $id, content: $content) {
            content
        }
    }
`;

export default compose(
    withState('currentContent', 'setContent', ({ staticContent }) => staticContent.content),
    withState('saving', 'setSaving', false),
    withHandlers({
        handleChange: ({ setContent }) => event => setContent(event.target.value),
        requestUpdate: ({ update }) => () => update(),
    }),
    graphql(mutation, {
        name: 'updateStatic',
        options: ({ staticContent }) => ({
            update: (proxy, { data: { updateStatic } }) => {
                const data = proxy.readQuery({ query: loadStaticQuery, variables: { code: staticContent.code } });
                data.Static.content = updateStatic.content;
                proxy.writeQuery({ query: loadStaticQuery, data });
            },
        }),
        props: ({ updateStatic, ownProps: { setSaving, staticContent, currentContent, onClose } }) => ({
            requestUpdate: () => {
                setSaving(true);
                updateStatic({
                    variables: { id: staticContent.id, content: currentContent },
                }).then(() => {
                    setSaving(false);
                    onClose();
                });
            },
        }),
    }),
)(StaticEdition);
