import styled from 'styled-components';

export default styled.h1`
    color: orange;
    margin: 0;
    width: 100%;
    font-size: 2rem;
    font-family: 'boycott';
    margin-top: 1rem;
    text-align: center;
    text-shadow: 0 0 1rem black;

    @media (min-width: 601px) {
        font-size: 3rem;
    }
`;
