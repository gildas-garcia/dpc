import React from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import styled from 'styled-components';
import styledProperty from 'styled-property';

import { Button, LinkButton } from '../components/forms';
import { signOff } from '../lib/auth';
import dpcPropTypes from '../lib/propTypes';

const Modal = styled(ReactModal)`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: space-between;
    position: absolute;
    top: 1rem;
    left: 1rem;
    right: 1rem;
    bottom: 1rem;
    border: none;
    background: transparent;
    overflow: auto;
    outline: none;
    padding: 1rem;
`;

const ModalWithOverlay = styledProperty(Modal, 'overlayClassName')`
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(0, 0, 0, 0.85);
    z-index: 10000;
`;

const MenuButton = LinkButton.extend`margin-bottom: 1rem;`;

const CloseButton = Button.extend`margin-top: auto;`;

const NavMenu = ({ onClose, show, user }) => (
    <ModalWithOverlay isOpen={show} contentLabel="Menu">
        <MenuButton primary prefetch href="/trainings">
            Entraînements
        </MenuButton>
        <MenuButton primary prefetch href="/contact">
            Nous contacter
        </MenuButton>

        {!user && (
            <MenuButton primary prefetch href="/auth/sign-in">
                Connexion
            </MenuButton>
        )}
        {user && (
            <Button primary onClick={signOff}>
                Deconnexion
            </Button>
        )}
        <CloseButton primary onClick={onClose}>
            Fermer
        </CloseButton>
    </ModalWithOverlay>
);

NavMenu.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    user: dpcPropTypes.user,
};

export default NavMenu;
