import styled from 'styled-components';
import styledProperty from 'styled-property';
import ReactModal from 'react-modal';

export const Modal = styled(ReactModal)`
    position: absolute;

    top: 0.5rem;
    left: 0.5rem;
    right: 0.5rem;
    padding: 0.5rem;

    @media (min-width: 601px) {
        top: 4rem;
        left: 4rem;
        right: 4rem;
        padding: 1rem;
    }
    border: 1px solid rgb(204, 204, 204);
    background: rgb(255, 255, 255);
    overflow: auto;
    border-radius: 0.25rem;
    outline: none;
`;

export const ModalWithOverlay = styledProperty(Modal, 'overlayClassName')`
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(0, 0, 0, 0.80);
    z-index: 10000;
`;
