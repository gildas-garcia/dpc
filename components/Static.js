import React from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import { gql, graphql } from 'react-apollo';
import compose from 'recompose/compose';
import getContext from 'recompose/getContext';
import withState from 'recompose/withState';
import withProps from 'recompose/withProps';
import withHandlers from 'recompose/withHandlers';
import dynamic from 'next/dynamic';

import dpcPropTypes from '../lib/propTypes';
import { Button } from './forms';

const StaticEdition = dynamic(import('./StaticEdition'), { ssr: false });

const Static = ({ canEdit, className, edition, loading, staticContent, toggleEdition }) =>
    loading || !staticContent ? null : (
        <div className={className}>
            {canEdit && (
                <Button primary onClick={toggleEdition}>
                    Modifier
                </Button>
            )}
            <ReactMarkdown source={staticContent.content} />
            {edition && <StaticEdition staticContent={staticContent} onClose={toggleEdition} />}
        </div>
    );

Static.propTypes = {
    canEdit: PropTypes.bool,
    className: PropTypes.string,
    edition: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    staticContent: PropTypes.object,
    toggleEdition: PropTypes.func.isRequired,
};

export const loadStaticQuery = gql`
    query loadStatic($code: String!) {
        Static(code: $code) {
            id
            code
            content
        }
    }
`;

export default compose(
    getContext({ user: dpcPropTypes.user }),
    withProps(({ user }) => ({
        canEdit: user && !!user.permissions.find(p => p.type === 'EDIT_STATICS'),
    })),
    withState('edition', 'setEdition', false),
    withHandlers({
        toggleEdition: ({ edition, setEdition, canEdit }) => () => {
            if (!canEdit) return;
            setEdition(!edition);
        },
    }),
    graphql(loadStaticQuery, {
        options: ({ code }) => ({
            variables: { code },
        }),
        props: ({ data: { loading, Static } }) => ({ loading, staticContent: Static }),
    }),
)(Static);
