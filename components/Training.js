import React from 'react';
import PropTypes from 'prop-types';
import getContext from 'recompose/getContext';
import compose from 'recompose/compose';
import withState from 'recompose/withState';
import withProps from 'recompose/withProps';
import withHandlers from 'recompose/withHandlers';
import styled from 'styled-components';
import { gql, graphql } from 'react-apollo';

import TrainingDelete from './TrainingDelete';
import TrainingEdition from './TrainingEdition';
import { Button } from './forms';
import dpcPropTypes from '../lib/propTypes';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    border-bottom: ${props => (props.last ? 'none' : '1px solid black')};
    padding-top: 1rem;
    padding-bottom: 1rem;
    margin-left: 2rem;
`;

const TrainingContainer = styled.div`
    display: flex;
    flex-direction: column;
    @media (min-width: 601px) {
        flex-direction: row;
    }
    justify-content: space-between;
`;

const EditionContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    margin-top: 0.5rem;
`;

const TrainingPublic = styled.div`
    margin-right: auto;
    font-style: italic;
    flex-grow: 0;
    @media (min-width: 601px) {
        max-width: 50%;
    }
`;

const TrainingData = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 0.5rem;

    @media (min-width: 601px) {
        flex-direction: row;
        margin-right: 0.5rem;
    }
    flex-grow: 2;
    justify-content: flex-end;
`;

const TrainingTime = styled.div`
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
    flex-grow: 0;

    @media (min-width: 601px) {
        margin-top: 0;
        margin-bottom: 0;
        white-space: nowrap;

        &:after {
            content: ' - ';
            margin-left: 0.25rem;
            margin-right: 0.5rem;
        }
    }
`;

const TrainingLocation = styled.div`
    margin-bottom: 0.5rem;
    flex-grow: 0;

    @media (min-width: 601px) {
        margin-bottom: 0;
        white-space: nowrap;

        &:after {
            content: ' - ';
            margin-left: 0.25rem;
            margin-right: 0.5rem;
        }
    }
`;

const TrainingPrice = styled.div``;

const Training = ({ canEdit, deleteTraining, edition, last, requestDelete, toggleDelete, toggleEdition, training }) => (
    <Container last={last}>
        <TrainingContainer>
            <TrainingPublic>{training.public}</TrainingPublic>
            <TrainingData>
                <TrainingTime>{training.time}</TrainingTime>
                <TrainingLocation>{training.location}</TrainingLocation>
                <TrainingPrice>{training.yearPrice}€/an</TrainingPrice>
            </TrainingData>
        </TrainingContainer>
        <EditionContainer>
            {canEdit && (
                <Button primary onClick={toggleEdition}>
                    Modifier
                </Button>
            )}
            {canEdit && <Button onClick={toggleDelete}>Supprimer</Button>}
            <TrainingEdition toggleEdition={toggleEdition} show={edition} training={training} />
            <TrainingDelete
                onCancel={toggleDelete}
                onConfirm={deleteTraining}
                show={requestDelete}
                training={training}
            />
        </EditionContainer>
    </Container>
);

Training.propTypes = {
    canEdit: PropTypes.bool,
    deleteTraining: PropTypes.func.isRequired,
    edition: PropTypes.bool,
    last: PropTypes.bool,
    requestDelete: PropTypes.bool,
    toggleEdition: PropTypes.func.isRequired,
    toggleDelete: PropTypes.func.isRequired,
    training: PropTypes.object.isRequired,
};

const deleteTrainingQuery = gql`
    mutation($id: ID!) {
        deleteTraining(id: $id) {
            id
        }
    }
`;

export default compose(
    getContext({ user: dpcPropTypes.user }),
    withProps(({ user }) => ({
        canEdit: user && !!user.permissions.find(p => p.type === 'EDIT_TRAININGS'),
    })),
    withState('edition', 'setEdition', false),
    withState('requestDelete', 'setRequestDelete', false),
    graphql(deleteTrainingQuery, { name: 'deleteTraining', options: { refetchQueries: ['allTrainings'] } }),
    withHandlers({
        toggleDelete: ({ requestDelete, setRequestDelete, canEdit }) => () => {
            if (!canEdit) return;
            setRequestDelete(!requestDelete);
        },
        toggleEdition: ({ edition, setEdition, canEdit }) => () => {
            if (!canEdit) return;
            setEdition(!edition);
        },
        deleteTraining: ({ deleteTraining, setRequestDelete, training: { id } }) => () => {
            deleteTraining({ variables: { id } }).then(({ errors }) => {
                if (errors && errors.length > 0) {
                    throw new Error(errors[0].message);
                }

                setRequestDelete(false);
            });
        },
    }),
)(Training);
