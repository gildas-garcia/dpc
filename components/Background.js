import React from 'react';
import styled, { keyframes } from 'styled-components';
import LazyLoad from 'react-lazyload';

const slideAnimation = keyframes`
    0% {
	    opacity: 0;
	    animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    transform: scale(1.05);
	    animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    transform: scale(1.1) rotate(3deg);
	}
	22% {
		opacity: 0;
		transform: scale(1.1) translateY(-20%);
	}
	25% {
	    opacity: 0;
	    transform: scale(1.1) translateY(-100%) rotate(3deg);
	}
	100% { opacity: 0 }
`;

const List = styled.ul`
    list-style-type: none;
    margin: 0;
    padding: 0;
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    z-index: 0;

    &:after {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        z-index: 0;
        content: '';
        background: transparent url(/static/images/pattern.png) repeat top left;
    }

    li:nth-child(1) span {
        opacity: 1;
        background-image: url(/static/images/1.jpg);
    }
    li:nth-child(2) span {
        background-image: url(/static/images/2.jpg);
        animation-delay: 6s;
    }
    li:nth-child(3) span {
        background-image: url(/static/images/3.jpg);
        animation-delay: 12s;
    }
    li:nth-child(4) span {
        background-image: url(/static/images/4.jpg);
        animation-delay: 18s;
    }
    li:nth-child(5) span {
        background-image: url(/static/images/5.jpg);
        animation-delay: 24s;
    }
    li:nth-child(6) span {
        background-image: url(/static/images/6.jpg);
        animation-delay: 30s;
    }

    li:nth-child(2) div {
        animation-delay: 6s;
    }
    li:nth-child(3) div {
        animation-delay: 12s;
    }
    li:nth-child(4) div {
        animation-delay: 18s;
    }
    li:nth-child(5) div {
        animation-delay: 24s;
    }
    li:nth-child(6) div {
        animation-delay: 30s;
    }
`;

const Slide = styled.span`
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
    color: transparent;
    background-size: cover;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    opacity: 0;
    z-index: 0;
    animation: ${slideAnimation} 36s linear infinite 0s;
`;

const BackgroundPlaceholder = styled.div`
    margin: 0;
    padding: 0;
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    z-index: 0;
    opacity: 1;
    background-image: url(/static/images/1.jpg);
`;

const Background = () => (
    <LazyLoad once placeholder={<BackgroundPlaceholder />}>
        <List>
            <li>
                <Slide />
            </li>
            <li>
                <Slide />
            </li>
            <li>
                <Slide />
            </li>
            <li>
                <Slide />
            </li>
            <li>
                <Slide />
            </li>
            <li>
                <Slide />
            </li>
        </List>
    </LazyLoad>
);

export default Background;
