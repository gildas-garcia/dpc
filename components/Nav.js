import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import getContext from 'recompose/getContext';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import withProps from 'recompose/withProps';
import { translate } from 'react-polyglot';
import styled from 'styled-components';
import Media from 'react-media';
import NoSSR from 'react-no-ssr';
import Router from 'next/router';

import Link from './Link';
import LightBox from './LightBox';
import { signOff } from '../lib/auth';
import dpcPropTypes from '../lib/propTypes';
import NavMenu from './NavMenu';

const NavBarLightBox = LightBox.extend`
    position: fixed;
    top: 1rem;
    left: 0;
    right: 0;
`;

const NavBar = NavBarLightBox.withComponent('nav');

const NavBarList = styled.ul`
    display: flex;
    justify-content: space-between;
    padding: 0;
`;

const NavBarListItem = styled.li`
    display: flex;
    margin: 0 1rem;
`;

const NavBarLink = styled(Link)`
    color: ${({ active }) => (active ? 'orange' : 'black')};
    cursor: pointer;
    text-decoration: none;
    font-family: 'boycott';
    font-size: 2rem;
    line-height: 2rem;
    border: none;
    background-color: transparent;
    outline: none;
    padding: 0;

    &:hover {
        color: orange;
    }
`;

const NavBarButton = NavBarLink.withComponent('button');

const User = styled.div`
    position: absolute;
    right: 1.5rem;
    bottom: 0;
`;

const Nav = ({ links, showMenu, t, toggleMenu, user }) => (
    <NavBar>
        <NavBarList>
            <NavBarListItem>
                <NavBarLink prefetch href="/">
                    {t('nav.home')}
                </NavBarLink>
            </NavBarListItem>
            <NoSSR>
                <Media query="(max-width: 40em)">
                    <NavBarList>
                        <NavBarListItem>
                            <NavBarButton onClick={toggleMenu}>{t('nav.menu')}</NavBarButton>
                        </NavBarListItem>
                        <NavMenu onClose={toggleMenu} show={showMenu} user={user} />
                    </NavBarList>
                </Media>
            </NoSSR>
            <NoSSR>
                <Media query="(min-width: 601px)">
                    <NavBarList>
                        {links.map(({ active, key, href, label }) => (
                            <NavBarListItem key={key}>
                                <NavBarLink active={active} prefetch href={href}>
                                    {t(label)}
                                </NavBarLink>
                            </NavBarListItem>
                        ))}
                        {!user && (
                            <NavBarListItem>
                                <NavBarLink prefetch href="/auth/sign-in">
                                    {t('nav.sign-in')}
                                </NavBarLink>
                            </NavBarListItem>
                        )}
                        {user && (
                            <NavBarListItem>
                                <NavBarButton onClick={signOff}>{t('nav.sign-off')}</NavBarButton>
                            </NavBarListItem>
                        )}
                    </NavBarList>
                </Media>
            </NoSSR>
        </NavBarList>
        {user && <User>{user.displayName}</User>}
    </NavBar>
);

Nav.propTypes = {
    links: PropTypes.arrayOf(
        PropTypes.shape({
            active: PropTypes.bool.isRequired,
            href: PropTypes.string.isRequired,
            key: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
        }),
    ).isRequired,
    showMenu: PropTypes.bool.isRequired,
    t: PropTypes.func.isRequired,
    toggleMenu: PropTypes.func.isRequired,
    user: dpcPropTypes.user,
};

export default compose(
    getContext({ user: dpcPropTypes.user }),
    translate(),
    withState('showMenu', 'setShowMenu', false),
    withHandlers({
        toggleMenu: ({ showMenu, setShowMenu }) => () => {
            setShowMenu(!showMenu);
        },
    }),
    withProps(() => ({
        links: [
            { href: '/trainings', label: 'nav.trainings' },
            { href: '/contact', label: 'nav.contact' },
        ].map(link => ({
            ...link,
            active: process.browser ? Router.route.indexOf(link.href) > -1 : false,
            key: `nav-link-${link.href}`,
        })),
    })),
)(Nav);
