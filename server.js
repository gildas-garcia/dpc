const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const next = require('next');
const compression = require('compression');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = express();
    server.use(compression());
    server.use(bodyParser.json());
    server.use(cookieParser());

    server.use(
        '/static',
        express.static(`${__dirname}/static`, {
            maxAge: '365d',
        }),
    );

    server.get('*', (req, res) => {
        return handle(req, res);
    });

    server.listen(3000, err => {
        if (err) throw err;
    });
});
