import { gql, graphql } from 'react-apollo';

export const listTrainingQuery = gql`
    query allTrainings {
        data: allTrainings {
            id
            days
            location
            public
            time
            yearPrice
        }
    }
`;

export const Days = {
    MONDAY: 'MONDAY',
    TUESDAY: 'TUESDAY',
    WEDNESDAY: 'WEDNESDAY',
    THURSDAY: 'THURSDAY',
    FRIDAY: 'FRIDAY',
    SATURDAY: 'SATURDAY',
    SUNDAY: 'SUNDAY',
};

export default Component =>
    graphql(listTrainingQuery, {
        props: ({ data: { loading, data = [] } }) => {
            return {
                loading,
                trainingDays: Object.keys(Days)
                    .map(day => ({
                        day,
                        trainings: data.filter(t => t.days.indexOf(day) > -1),
                    }))
                    .filter(trainingDay => trainingDay.trainings.length > 0),
            };
        },
    })(Component);
