.PHONY: build help

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

copy-conf: ## Initialize the configuration files
	-cp -n ./config/development-dist.js ./config/development.js

install: package.json ## Install dependencies
	@yarn

build: ## Build the app for production
	@NODE_ENV=production yarn build

test: ## Launch unit tests

start: ## Start the app in development mode
	@yarn dev

deploy:
	@yarn deploy
