import React, { Component } from 'react';
import validate from 'validate.js';

export default (schema, submit, getInitialValues = () => ({})) => ComposedComponent => {
    return class withForm extends Component {
        state = {
            isValid: false,
            isSubmitting: false,
            submitFailed: false,
            submitSucceeded: false,
            values: {},
            validation: Object.keys(schema).reduce(
                (acc, key) => ({
                    ...acc,
                    [key]: {
                        touched: false,
                        errors: undefined,
                    },
                }),
                {},
            ),
        };

        componentDidMount() {
            const initialValues = getInitialValues(this.props);
            const isValid = !validate(initialValues, schema);

            this.setState({
                isInvalid: !isValid,
                isValid: isValid,
                values: Object.keys(schema).reduce(
                    (acc, key) => ({
                        ...acc,
                        [key]: initialValues[key],
                    }),
                    {},
                ),
            });
        }

        handleFieldChange = event => {
            const value = event.target.value;
            const name = event.target.name;

            const values = {
                ...this.state.values,
                [name]: value,
            };
            const errors = validate.single(value, schema[name]);
            const isValid = !validate(values, schema);

            const validation = {
                ...this.state.validation,
                [name]: {
                    touched: true,
                    errors,
                },
            };

            this.setState({
                isValid,
                validation,
                values,
            });
        };

        handleSubmit = event => {
            event.preventDefault();

            if (!this.state.isValid) {
                return;
            }

            this.setState({ isSubmitting: true, submitFailed: false, submitSucceeded: false });

            submit(this.state.values, this.props)
                .then(() => {
                    this.setState({ isSubmitting: false, submitSucceeded: true });
                })
                .catch(error => {
                    console.error(error);
                    this.setState({ isSubmitting: false, submitFailed: true });
                });
        };

        render() {
            const { isSubmitting, isValid, submitFailed, submitSucceeded, validation, values } = this.state;

            return (
                <ComposedComponent
                    {...this.props}
                    isValid={isValid}
                    isInvalid={!isValid}
                    isSubmitting={isSubmitting}
                    submitFailed={submitFailed}
                    submitSucceeded={submitSucceeded}
                    onFieldChange={this.handleFieldChange}
                    onSubmit={this.handleSubmit}
                    validation={validation}
                    values={values}
                />
            );
        }
    };
};
