const days = [
    {
        index: 0,
        day: 'lundi',
    },
    {
        index: 1,
        day: 'mardi',
    },
    {
        index: 2,
        day: 'mercredi',
    },
    {
        index: 3,
        day: 'jeudi',
    },
    {
        index: 4,
        day: 'vendredi',
    },
    {
        index: 5,
        day: 'samedi',
    },
    {
        index: 6,
        day: 'dimanche',
    },
];

export default (day1, day2) => {
    const realDay1 = day1.split(' ')[0].toLowerCase();
    const realDay2 = day2.split(' ')[0].toLowerCase();

    const day1Index = days.find(d => d.day === realDay1).index;
    const day2Index = days.find(d => d.day === realDay2).index;

    return day1Index > day2Index;
};
