import { ApolloClient, createNetworkInterface } from 'react-apollo';
import fetch from 'isomorphic-fetch';
import { getUserToken } from './auth';

let apolloClient = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
    global.fetch = fetch;
}

const authorizationMiddleware = ctx => ({
    applyMiddleware(req, next) {
        if (!req.options.headers) {
            req.options.headers = {};
        }

        const authorization = getUserToken(ctx && ctx.req);

        if (authorization) {
            req.options.headers.authorization = `Bearer ${authorization}`;
        }
        next();
    },
});

function create(initialState, ctx) {
    const networkInterface = createNetworkInterface({
        uri: 'https://api.graph.cool/simple/v1/cj3h6vilxkzr40170u6f0sp4f',
        opts: {
            credentials: 'same-origin',
        },
    });

    networkInterface.use([authorizationMiddleware(ctx)]);

    return new ApolloClient({
        initialState,
        ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
        networkInterface,
    });
}

export default function initApollo(initialState, ctx) {
    // Make sure to create a new client for every server-side request so that data
    // isn't shared between connections (which would be bad)
    if (!process.browser) {
        return create(initialState, ctx);
    }

    // Reuse client on the client-side
    if (!apolloClient) {
        apolloClient = create(initialState);
    }

    return apolloClient;
}
