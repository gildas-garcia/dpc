import PropTypes from 'prop-types';

export const userPropType = PropTypes.shape({
    displayName: PropTypes.string.isRequired,
    permissions: PropTypes.arrayOf(
        PropTypes.shape({
            type: PropTypes.string.isRequired,
        }),
    ),
});

export default {
    user: userPropType,
};
