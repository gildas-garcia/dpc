import { gql, graphql } from 'react-apollo';
import compose from 'recompose/compose';
import withContext from 'recompose/withContext';
import dpcPropTypes from './propTypes';

export default ComposedComponent => {
    const userQuery = gql`
        query loadUser {
            user {
                displayName
                permissions {
                    type
                }
            }
        }
    `;

    const EnhancedComponent = compose(
        graphql(userQuery, {
            options: { fetchPolicy: 'network-only' },
            props: ({ data: { loading, user } }) => ({
                loadingUser: loading,
                user,
            }),
        }),
        withContext({ user: dpcPropTypes.user }, ({ user }) => ({ user })),
    )(ComposedComponent);

    EnhancedComponent.getInitialProps = ComposedComponent.getInitialProps;

    return EnhancedComponent;
};
