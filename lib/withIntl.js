import React, { Component } from 'react';
import { I18n, translate } from 'react-polyglot';
import messages from '../i18n/fr';

export default ComposedComponent => {
    const I18nComposedComponent = translate()(ComposedComponent);

    return class withIntl extends Component {
        state = {
            locale: 'fr',
        };

        render() {
            const { locale } = this.state;

            return (
                <I18n locale={locale} messages={messages}>
                    <I18nComposedComponent {...this.props} />
                </I18n>
            );
        }
    };
};
