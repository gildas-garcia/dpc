export default (name, src, fontWeight = 'normal', fontStyle = 'normal') => `
    @font-face {
        font-family: "${name}";
        src: url(/static/fonts/${name}.eot);
        src: url(/static/fonts/${name}.eot?#iefix) format("embedded-opentype"),
                url(/static/fonts/${name}.woff) format("woff"),
                url(/static/fonts/${name}.ttf) format("truetype"),
                url(/static/fonts/${name}.svg#${name}) format("svg");

        font-style: ${fontStyle};
        font-weight: ${fontWeight};
    }
`;
