import React, { Component } from 'react';
import { injectGlobal } from 'styled-components';
import NoSSR from 'react-no-ssr';
import fontFace from '../lib/fontFace';
import Background from '../components/Background';
export default ComposedComponent => {
    return class withGlobalStyles extends Component {
        componentWillMount() {
            injectGlobal`
                ${fontFace('boycott')}

                body {
                    margin: 0;
                    font-family: -apple-system,BlinkMacSystemFont,Avenir Next,Avenir,Helvetica,sans-serif;
                    overflow-y: auto;
                    overflow-x: hidden;                    
                }
            `;
        }
        render() {
            return (
                <div>
                    <NoSSR>
                        <Background />
                    </NoSSR>
                    <div style={{ position: 'relative', zIndex: 9999 }}>
                        <ComposedComponent {...this.props} />
                    </div>
                </div>
            );
        }
    };
};
