import Cookies from 'js-cookie';

export const signIn = ({ email, token }) => {
    window.localStorage.setItem('email', email);
    window.localStorage.setItem('graphcoolToken', token);
    Cookies.set('graphcoolToken', token);
};

export const getUserToken = req => {
    if (process.browser) {
        return window.localStorage.getItem('graphcoolToken');
    }

    if (!req.cookies) {
        return undefined;
    }

    return req.cookies['graphcoolToken'];
};

export const signOff = () => {
    window.localStorage.removeItem('graphcoolToken');
    window.localStorage.removeItem('email');
    location.reload();
};
