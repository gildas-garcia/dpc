import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';

import withData from '../lib/withData';
import withAuth from '../lib/withAuth';
import withGlobalStyles from '../lib/withGlobalStyles';
import withIntl from '../lib/withIntl';
import HeroCard from '../components/HeroCard';
import Layout from '../components/Layout';
import TrainingDaysList from '../components/TrainingDaysList';
import Static from '../components/Static';

export const Trainings = ({ t }) => (
    <Layout title={t('trainings.title')}>
        <HeroCard>
            <TrainingDaysList />
            <Static code="subscriptions" />
        </HeroCard>
    </Layout>
);

Trainings.propTypes = {
    t: PropTypes.func.isRequired,
};

export default compose(withData, withAuth, withGlobalStyles, withIntl)(Trainings);
