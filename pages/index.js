import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import styled from 'styled-components';
import Media from 'react-media';
import NoSSR from 'react-no-ssr';

import withData from '../lib/withData';
import withAuth from '../lib/withAuth';
import withGlobalStyles from '../lib/withGlobalStyles';
import withIntl from '../lib/withIntl';
import Layout from '../components/Layout';
import HeroTitle from '../components/HeroTitle';
import HeroSubTitle from '../components/HeroSubTitle';
import { LinkButton } from '../components/forms';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 2;
    margin-top: 5rem;

    @media (min-width: 601px) {
        margin-top: 10rem;
    }
    padding-top: 2rem;
`;

const MobileContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: flex-end;
    margin-top: auto;
    flex-grow: 2;
`;

const Button = LinkButton.extend`margin-bottom: 1rem;`;

export const Home = ({ t }) => (
    <Layout title={t('home.title')}>
        <Container>
            <HeroTitle>{t('home.title')}</HeroTitle>
            <HeroSubTitle>{t('home.catch-phrase')}</HeroSubTitle>

            <NoSSR>
                <Media query="(max-width: 40em)">
                    <MobileContainer>
                        <Button primary prefetch href="/trainings">
                            {t('trainings.title')}
                        </Button>
                        <Button primary prefetch href="/contact">
                            {t('contact.title')}
                        </Button>
                    </MobileContainer>
                </Media>
            </NoSSR>
        </Container>
    </Layout>
);

Home.propTypes = {
    t: PropTypes.func.isRequired,
};

export default compose(withData, withAuth, withGlobalStyles, withIntl)(Home);
