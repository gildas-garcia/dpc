import React from 'react';
import PropTypes from 'prop-types';
import { gql, graphql } from 'react-apollo';
import compose from 'recompose/compose';
import Router from 'next/router';

import withData from '../../lib/withData';
import withAuth from '../../lib/withAuth';
import withGlobalStyles from '../../lib/withGlobalStyles';
import withIntl from '../../lib/withIntl';
import withValidation from '../../lib/withValidation';
import { signIn } from '../../lib/auth';
import Layout from '../../components/Layout';
import HeroCard from '../../components/HeroCard';
import { Error, Form, FormGroup, LinkButton, TextInput, Submit, Success } from '../../components/forms';
import Spinner from '../../components/Spinner';
import Separator from '../../components/Separator';

const createUser = gql`
    mutation($email: String!, $password: String!) {
        createUser(authProvider: { email: { email: $email, password: $password } }) {
            id
        }
    }
`;

const signinUser = gql`
    mutation($email: String!, $password: String!) {
        signinUser(email: { email: $email, password: $password }) {
            token
        }
    }
`;

export const SignUp = ({
    onFieldChange,
    onSubmit,
    isValid,
    isSubmitting,
    submitSucceeded,
    submitFailed,
    validation,
    values: { email, password },
    t,
}) => (
    <Layout title={t('sign-up.title')}>
        <HeroCard>
            <Form noValidate onSubmit={onSubmit}>
                <div>
                    <div>{t('sign-up.are-you-a-member')}</div>
                    <p>{t('sign-up.register')}</p>
                    <p>{t('sign-up.instructions')}</p>
                </div>
                <Separator />
                <FormGroup>
                    <label htmlFor="email">{t('sign-up.email')}</label>
                    <TextInput id="email" name="email" type="email" required value={email} onChange={onFieldChange} />
                    {validation.email.touched &&
                    validation.email.errors && <Error>{t(validation.email.errors[0])}</Error>}
                </FormGroup>

                <FormGroup>
                    <label htmlFor="password">{t('sign-up.password')}</label>
                    <TextInput
                        id="password"
                        name="password"
                        type="password"
                        required
                        value={password}
                        onChange={onFieldChange}
                    />
                    {validation.password.touched &&
                    validation.password.errors && <Error>{t(validation.password.errors[0])}</Error>}
                </FormGroup>
                <FormGroup>
                    <Submit primary disabled={!isValid || isSubmitting} type="submit">
                        {t('sign-up.sign-up')}
                    </Submit>
                    <Spinner show={isSubmitting} />
                    {submitSucceeded && <Success inline>{t('sign-up.success')}</Success>}
                    {submitFailed && <Error inline>{t('sign-up.error')}</Error>}
                    <LinkButton prefetch href="/auth/sign-in">
                        {t('sign-up.have-account')}
                    </LinkButton>
                </FormGroup>
            </Form>
        </HeroCard>
    </Layout>
);

SignUp.propTypes = {
    values: PropTypes.shape({
        email: PropTypes.string,
        password: PropTypes.string,
    }),
    isSubmitting: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
    onFieldChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    submitFailed: PropTypes.bool.isRequired,
    submitSucceeded: PropTypes.bool.isRequired,
    t: PropTypes.func.isRequired,
    validation: PropTypes.shape({
        email: PropTypes.shape({
            touched: PropTypes.bool.isRequired,
            errors: PropTypes.arrayOf(PropTypes.string),
        }),
        password: PropTypes.shape({
            touched: PropTypes.bool.isRequired,
            errors: PropTypes.arrayOf(PropTypes.string),
        }),
    }),
};

export default compose(
    withData,
    withAuth,
    withGlobalStyles,
    withIntl,
    graphql(createUser, { name: 'createUser' }),
    graphql(signinUser, { name: 'signinUser' }),
    withValidation(
        {
            email: {
                email: { message: 'sign-in.errors.email' },
                presence: { message: 'sign-in.errors.required' },
            },
            password: {
                presence: { message: 'sign-in.errors.required' },
            },
        },
        (variables, { createUser, signinUser }) =>
            createUser(
                { variables },
                {
                    props: ({ data: { loading } }) => ({
                        loading,
                    }),
                },
            ).then(() =>
                signinUser(
                    { variables },
                    {
                        props: ({ data: { loading } }) => ({
                            loading,
                        }),
                    },
                ).then(response => {
                    signIn({
                        email: variables.email,
                        token: response.data.signinUser.token,
                    });
                    Router.push('/');
                    return true;
                }),
            ),
    ),
)(SignUp);
