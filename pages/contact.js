import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import styled from 'styled-components';

import withData from '../lib/withData';
import withAuth from '../lib/withAuth';
import withGlobalStyles from '../lib/withGlobalStyles';
import withIntl from '../lib/withIntl';
import Layout from '../components/Layout';
import HeroCard from '../components/HeroCard';

const Title = styled.h2`
    font-weight: bold;
    border-bottom: 1px solid black;
`;

const ContactType = styled.dt`
    font-weight: bold;
    margin: 0.5rem 0;
`;
const Content = styled.dd`margin: 0.5rem 0 2rem 2rem;`;

const Address = styled.address`margin: 0.5rem 0 0;`;

export const Contacts = ({ t }) => (
    <Layout title={t('contact.title')}>
        <HeroCard>
            <Title>{t('contact.title')}</Title>
            <dl>
                <ContactType>{t('contact.email')}:</ContactType>
                <Content>
                    <a href="mailto:parkour.dijon@gmail.com">parkour.dijon@gmail.com</a>
                </Content>
                <ContactType>{t('contact.phone.president')}:</ContactType>
                <Content>
                    <a href="tel:0760276525">07 60 27 65 25</a>
                </Content>
                <ContactType>{t('contact.phone.coach')}:</ContactType>
                <Content>
                    <a href="tel:0610863512">06 10 86 35 12</a>
                </Content>
                <ContactType>{t('contact.facebook')}:</ContactType>
                <Content>
                    <a href="https://www.facebook.com/pg/AssoDPC/community/">Association Dijon Parkour Crew</a>
                </Content>
                <ContactType>{t('contact.address')}:</ContactType>
                <Content>
                    <Address>
                        Boîte DD3 <br />
                        Maison des Associations <br />
                        2 rue des Corroyeurs <br />
                        21000 Dijon, France
                    </Address>
                </Content>
            </dl>
        </HeroCard>
    </Layout>
);

Contacts.propTypes = {
    t: PropTypes.func.isRequired,
};

export default compose(withData, withAuth, withGlobalStyles, withIntl)(Contacts);
