# dpc

The website for the Dijon Parkour Crew association (french).

This project was bootstrapped with [Create Next App](https://github.com/segmentio/create-next-app).

It uses:

- next.js
- apollo
- graphcool

## Development

```sh
make install # uses yarn
# or
npm install
make copy-conf
```

Start the app with:

```sh
make start
# or
npm run dev
# or
yarn dev
```
